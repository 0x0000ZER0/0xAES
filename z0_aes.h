#ifndef Z0_AES_H
#define Z0_AES_H

#include <stdint.h>
#include <stddef.h>

#define Z0_AES_128

void
z0_aes_enc(uint8_t*, const uint8_t*, const uint8_t*);

void
z0_aes_dec(uint8_t*, const uint8_t*, const uint8_t*);

void
z0_aes_enc_ecb(uint8_t*, const uint8_t*, size_t, const uint8_t*);

void
z0_aes_dec_ecb(uint8_t*, const uint8_t*, size_t, const uint8_t*);

void
z0_aes_enc_cbc(uint8_t*, const uint8_t*, size_t, const uint8_t*, const uint8_t*);

void
z0_aes_dec_cbc(uint8_t*, const uint8_t*, size_t, const uint8_t*, const uint8_t*);

#endif
