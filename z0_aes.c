#include "z0_aes.h"

#include <string.h>

#ifdef Z0_AES_128
#define NR	10
#define NK	4
#define NB 	4
#endif

static const uint8_t sbox[] = {
	0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
	0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
	0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
	0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
	0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
	0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
	0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
	0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
	0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
	0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
	0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
	0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
	0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
	0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
	0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
	0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};

static const uint8_t sbox_inv[] = {
	0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
	0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
	0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
	0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
	0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
	0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
	0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
	0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
	0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
	0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
	0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
	0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
	0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
	0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
	0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
	0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D 
};

static const uint8_t rc[] = {
	0b00000001, // 1
	0b00000010, // x
	0b00000100, // x^2
	0b00001000, // x^3
	0b00010000, // x^4
	0b00100000, // x^5
	0b01000000, // x^6
	0b10000000, // x^7
	0b00011011, // x^8 mod (x^8 + x^4 + x^3 + x + 1) = x^4 + x^3 + x + 1
	0b00110110  // x^9 mod (x^8 + x^4 + x^3 + x + 1) = x^5 + x^4 + x^2 + x
};

static uint8_t
gf_mul(uint8_t a, uint8_t b)
{
	// x^8 + x^4 + x^3 + x + 1
	static const uint16_t poly = 0x11B;

	uint8_t res;
	res = 0;
	while (b > 0) {
		if (b & 1)
			res ^= a;

		if (a & 0x80)
			a = (a << 1) ^ poly;
		else
			a <<= 1;

		b >>= 1;
	}

	return res;
}

static void
aes_key_expand(uint8_t w[NB * NB * (NR + 1)], const uint8_t *key)
{
	memcpy(w, key, NB * NB);
	
	uint8_t t[4];

	uint_fast8_t i;
	for (i = 0; i < NR * 4; i += 4) {
		// g(x)
		t[0] = rc[i / 4] ^ sbox[w[(i + 3) * NB + 1]];
		t[1] = sbox[w[(i + 3) * NB + 2]];
		t[2] = sbox[w[(i + 3) * NB + 3]];
		t[3] = sbox[w[(i + 3) * NB + 0]];

		w[(i + 4) * NB + 0] = w[(i + 0) * NB + 0] ^ t[0];
		w[(i + 4) * NB + 1] = w[(i + 0) * NB + 1] ^ t[1];
		w[(i + 4) * NB + 2] = w[(i + 0) * NB + 2] ^ t[2];
		w[(i + 4) * NB + 3] = w[(i + 0) * NB + 3] ^ t[3];

		w[(i + 5) * NB + 0] = w[(i + 4) * NB + 0] ^ w[(i + 1) * NB + 0];
		w[(i + 5) * NB + 1] = w[(i + 4) * NB + 1] ^ w[(i + 1) * NB + 1];
		w[(i + 5) * NB + 2] = w[(i + 4) * NB + 2] ^ w[(i + 1) * NB + 2];
		w[(i + 5) * NB + 3] = w[(i + 4) * NB + 3] ^ w[(i + 1) * NB + 3];

		w[(i + 6) * NB + 0] = w[(i + 5) * NB + 0] ^ w[(i + 2) * NB + 0];
		w[(i + 6) * NB + 1] = w[(i + 5) * NB + 1] ^ w[(i + 2) * NB + 1];
		w[(i + 6) * NB + 2] = w[(i + 5) * NB + 2] ^ w[(i + 2) * NB + 2];
		w[(i + 6) * NB + 3] = w[(i + 5) * NB + 3] ^ w[(i + 2) * NB + 3];

		w[(i + 7) * NB + 0] = w[(i + 6) * NB + 0] ^ w[(i + 3) * NB + 0];
		w[(i + 7) * NB + 1] = w[(i + 6) * NB + 1] ^ w[(i + 3) * NB + 1];
		w[(i + 7) * NB + 2] = w[(i + 6) * NB + 2] ^ w[(i + 3) * NB + 2];
		w[(i + 7) * NB + 3] = w[(i + 6) * NB + 3] ^ w[(i + 3) * NB + 3];
	}
}

static void
aes_key_add(uint8_t *state, const uint8_t *key)
{
	uint_fast8_t i;
	uint_fast8_t j;

	for (i = 0; i < NB; ++i)
	for (j = 0; j < NB; ++j)
		state[i * NB + j] ^= key[i * NB + j];
}

static void
aes_sub_bytes(uint8_t *state)
{
	uint_fast8_t y, x;

	for (x = 0; x < NB; ++x)
	for (y = 0; y < NB; ++y)
		state[y * NB + x] = sbox[state[y * NB + x]];
}

static void
aes_sub_bytes_inv(uint8_t *state)
{
	uint_fast8_t y, x;

	for (x = 0; x < NB; ++x)
	for (y = 0; y < NB; ++y)
		state[y * NB + x] = sbox_inv[state[y * NB + x]];
}

static void
aes_shift_rows(uint8_t *state)
{
	uint8_t tmp;
	
	/*
	B0  B4  B8  B12       B0  B4  B8  B12 | << 0
	B1  B5  B9  B13  -->  B5  B9  B13 B1  | << 1
	B2  B6  B10 B14       B10 B14 B2  B6  | << 2
	B3  B7  B11 B15	      B15 B3  B7  B11 | << 3
	*/
	
	tmp = state[0 * NB + 1];
	state[0 * NB + 1] = state[1 * NB + 1];
	state[1 * NB + 1] = state[2 * NB + 1];
	state[2 * NB + 1] = state[3 * NB + 1];
	state[3 * NB + 1] = tmp;

	tmp = state[0 * NB + 2];
	state[0 * NB + 2] = state[2 * NB + 2];
	state[2 * NB + 2] = tmp; 
	tmp = state[1 * NB + 2];
	state[1 * NB + 2] = state[3 * NB + 2];
	state[3 * NB + 2] = tmp;

	tmp = state[0 * NB + 3];
	state[0 * NB + 3] = state[3 * NB + 3];
	state[3 * NB + 3] = state[2 * NB + 3];
	state[2 * NB + 3] = state[1 * NB + 3];
	state[1 * NB + 3] = tmp;
}

static void
aes_shift_rows_inv(uint8_t *state)
{
	uint8_t tmp;
	
	/*
	B0  B4  B8  B12        B0  B4  B8  B12  | >> 0
	B5  B9  B13 B1    -->  B1  B5  B9  B13  | >> 1
	B10 B14 B2  B6         B2  B6  B10 B14  | >> 2
	B15 B3  B7  B11        B3  B7  B11 B15  | >> 3
	*/
	
	tmp = state[3 * NB + 1];
	state[3 * NB + 1] = state[2 * NB + 1];
	state[2 * NB + 1] = state[1 * NB + 1];
	state[1 * NB + 1] = state[0 * NB + 1];
	state[0 * NB + 1] = tmp;

	tmp = state[0 * NB + 2];
	state[0 * NB + 2] = state[2 * NB + 2];
	state[2 * NB + 2] = tmp;
	tmp = state[1 * NB + 2];
	state[1 * NB + 2] = state[3 * NB + 2];
	state[3 * NB + 2] = tmp;

	tmp = state[0 * NB + 3];
	state[0 * NB + 3] = state[1 * NB + 3];
	state[1 * NB + 3] = state[2 * NB + 3];
	state[2 * NB + 3] = state[3 * NB + 3];
	state[3 * NB + 3] = tmp;
}

static void
aes_mix_cols(uint8_t *state)
{
	/*
	       4x1       4x4       4x1
	      --------------------------
		C0      2 3 1 1     B0
		C1  =   1 2 3 1  x  B1
		C2      1 1 2 3     B2
		C3      3 1 1 2     B3
	*/	
	uint8_t tmp[4];

	uint_fast8_t i;
	for (i = 0; i < NB; ++i) {
		tmp[0] = state[i * NB + 0];
		tmp[1] = state[i * NB + 1];
		tmp[2] = state[i * NB + 2];
		tmp[3] = state[i * NB + 3];

		state[i * NB + 0] = gf_mul(0x2, tmp[0]) ^ gf_mul(0x3, tmp[1]) ^ tmp[2] ^ tmp[3];
		state[i * NB + 1] = tmp[0] ^ gf_mul(0x2, tmp[1]) ^ gf_mul(0x3, tmp[2]) ^ tmp[3];
		state[i * NB + 2] = tmp[0] ^ tmp[1] ^ gf_mul(0x2, tmp[2]) ^ gf_mul(0x3, tmp[3]);
		state[i * NB + 3] = gf_mul(0x3, tmp[0]) ^ tmp[1] ^ tmp[2] ^ gf_mul(0x2, tmp[3]);
	}
}

static void
aes_mix_cols_inv(uint8_t *state)
{
	/*
	       4x1       4x4       4x1
	      --------------------------
		C0      E B D 9     B0
		C1  =   9 E B D  x  B1
		C2      D 9 E B     B2
		C3      B D 9 E     B3
	*/	
	uint8_t tmp[4];

	uint_fast8_t i;
	for (i = 0; i < NB; ++i) {
		tmp[0] = state[i * NB + 0];
		tmp[1] = state[i * NB + 1];
		tmp[2] = state[i * NB + 2];
		tmp[3] = state[i * NB + 3];

		state[i * NB + 0] = gf_mul(0xE, tmp[0]) ^ gf_mul(0xB, tmp[1]) ^ gf_mul(0xD, tmp[2]) ^ gf_mul(0x9, tmp[3]);
		state[i * NB + 1] = gf_mul(0x9, tmp[0]) ^ gf_mul(0xE, tmp[1]) ^ gf_mul(0xB, tmp[2]) ^ gf_mul(0xD, tmp[3]);
		state[i * NB + 2] = gf_mul(0xD, tmp[0]) ^ gf_mul(0x9, tmp[1]) ^ gf_mul(0xE, tmp[2]) ^ gf_mul(0xB, tmp[3]);
		state[i * NB + 3] = gf_mul(0xB, tmp[0]) ^ gf_mul(0xD, tmp[1]) ^ gf_mul(0x9, tmp[2]) ^ gf_mul(0xE, tmp[3]);
	}
}

void
z0_aes_enc(uint8_t *state, const uint8_t *block, const uint8_t *key)
{
	uint8_t w[NB * NB * (NR + 1)];
	aes_key_expand(w, key);	

	memcpy(state, block, NB * NB);
	aes_key_add(state, w);

	uint_fast8_t i;
	for (i = 1; i < NR; ++i) {
		aes_sub_bytes(state);
		aes_shift_rows(state);
		aes_mix_cols(state);
		aes_key_add(state, w + NK * NB * i);
	}

	aes_sub_bytes(state);
	aes_shift_rows(state);
	aes_key_add(state, w + NK * NB * 10);
}

void
z0_aes_dec(uint8_t *state, const uint8_t *block, const uint8_t *key)
{
	memcpy(state, block, NB * NB);

	uint8_t w[NB * NB * (NR + 1)];
	aes_key_expand(w, key);	
	aes_key_add(state, w + NK * NB * 10);
	aes_shift_rows_inv(state);
	aes_sub_bytes_inv(state);

	uint_fast8_t i;
	for (i = 9; i >= 1; --i) {
		aes_key_add(state, w + NK * NB * i);
		aes_mix_cols_inv(state);
		aes_shift_rows_inv(state);
		aes_sub_bytes_inv(state);
	}

	aes_key_add(state, w);
}

void
z0_aes_enc_ecb(uint8_t *out, const uint8_t *in, size_t len, const uint8_t *key)
{
	size_t n;
	n = len / (NB * NB);

	size_t r;
	r = len % (NB * NB);

	size_t i;
	for (i = 0; i < n; ++i)
		z0_aes_enc(out + i * NB * NB, in + i * NB * NB, key);

	uint8_t state[NB * NB];
	memcpy(state, in + i * NB * NB, r);
	memset(state + r, 0, sizeof (state) - r);

	z0_aes_enc(out + i * NB * NB, state, key);
} 

void
z0_aes_dec_ecb(uint8_t *out, const uint8_t *in, size_t len, const uint8_t *key)
{
	size_t i;
	for (i = 0; i < len; i += NB * NB)
		z0_aes_dec(out + i, in + i, key);
} 

void
z0_aes_enc_cbc(uint8_t *out, const uint8_t *in, size_t len, 
		const uint8_t *key, const uint8_t *iv)
{
	uint8_t vec[NB * NB];
	memcpy(vec, iv, sizeof (vec));

	size_t n;
	n = len / (NB * NB);

	size_t r;
	r = len % (NB * NB);

	uint8_t block[NB * NB];

	size_t i, j;
	for (i = 0; i < n; ++i) {
		memcpy(block, in + i * NB * NB, sizeof (block));
		for (j = 0; j < NB * NB; ++j)
			block[j] ^= vec[j];

		z0_aes_enc(out + i * NB * NB, block, key);
		memcpy(vec, out + i * NB * NB, sizeof (vec));
	}

	
	memcpy(block, in + i * NB * NB, r);
	memset(block + r, 0, sizeof (block) - r);
	for (j = 0; j < NB * NB; ++j)
		block[j] ^= vec[j];

	z0_aes_enc(out + i * NB * NB, block, key);
}

void
z0_aes_dec_cbc(uint8_t *out, const uint8_t *in, size_t len, 
		const uint8_t *key, const uint8_t *iv)
{
	uint8_t vec[NB * NB];
	memcpy(vec, iv, sizeof (vec));

	uint8_t block[NB * NB];

	size_t i, j;
	for (i = 0; i < len; i += NB * NB) {
		memcpy(block, out + i, sizeof (block));

		z0_aes_dec(out + i, in + i, key);
		for (j = 0; j < NB * NB; ++j)
			*(out + i) ^= vec[i];

		memcpy(vec, block, sizeof (vec));
	}
}
